use crate::cpu::Cpu;
use crate::mem::Ram;
use crate::START_ADDR;

pub struct Chip8 {
    cpu: Cpu,
    memory: Ram,
}

impl Chip8 {
    pub fn load_rom(&mut self, rom: &[u8]) -> std::io::Result<()> {
        for (i, b) in rom.iter().enumerate() {
            self.memory.write_byte(START_ADDR + i as u16, *b);
        }
        Ok(())
    }
    pub fn load_file(&mut self, path: &str) -> std::io::Result<()> {
        let file = std::fs::read(path)?;
        self.load_rom(&file)
    }

    pub fn read_thingymajigigigigigigigies(&mut self) {
        let ipone = self.memory.read_byte(self.cpu.instruction_pointer) as u16;
        let iptwo = self.memory.read_byte(self.cpu.instruction_pointer + 1) as u16;
        let opcode: u16 = ipone << 8 | iptwo;

        // increment instruction pointer before doing anything else
        self.cpu.instruction_pointer += 2;
        self.execute_opcode(opcode);

        // if delay timer > 0 decrease it?
        // if sound timer > 0 decrease it? if its been set at all
    }
    pub fn execute_opcode(&mut self, opcode: u16) {
        match opcode {
            0xE0 => {}
            0xEE => {
                println!("opcode {:#06X}", opcode);
                self.cpu.instruction_pointer = self.cpu.stack[self.cpu.sp as usize];
                self.cpu.sp -= 1;
            }
            0x1000..=0x1FFF => {
                println!("opcode {:#06X}", opcode);
                let address: u16 = opcode & 0x0FFF;
                self.cpu.instruction_pointer = address;
            }
            0x2000..=0x2FFF => {
                println!("opcode {:#06X}", opcode);
                let address: u16 = opcode & 0x0FFF;
                self.cpu.stack[self.cpu.sp as usize] = self.cpu.instruction_pointer;
                self.cpu.sp += 1;
                self.cpu.instruction_pointer = address;
            }
            0x3000..=0x3FFF => {
                println!("opcode {:#06X}", opcode);
                let vx: u8 = ((opcode & 0x0F00) >> 8) as u8;
                let byte: u8 = (opcode & 0x00FF) as u8;
                if self.cpu.read_register(vx) == byte {
                    self.cpu.instruction_pointer += 2;
                }
            }
            0x4000..=0x4FFF => {
                println!("opcode {:#06X}", opcode);
                let vx: u8 = ((opcode & 0x0F00) >> 8) as u8;
                let byte: u8 = (opcode & 0x00FF) as u8;
                if self.cpu.read_register(vx) != byte {
                    self.cpu.instruction_pointer += 2;
                }
            }
            0x5000..=0x5FFF => {
                println!("opcode {:#06X}", opcode);
                let vx: u8 = ((opcode & 0x0F00) >> 8) as u8;
                let vy: u8 = ((opcode & 0x00F0) >> 4) as u8;
                if self.cpu.read_register(vx) == self.cpu.read_register(vy) {
                    self.cpu.instruction_pointer += 2;
                }
            }
            0x6000..=0x6FFF => {
                println!("opcode {:#6X}", opcode);
                let vx: u8 = ((opcode & 0x0F00) >> 8) as u8;
                let byte: u8 = (opcode & 0x00FF) as u8;
                self.cpu.write_register(vx, byte);
            }
            0x7000..=0x7FFF => {
                println!("opcode {:#06X}", opcode);
                let vx: u8 = ((opcode & 0x0F00) >> 8) as u8;
                let byte: u8 = (opcode & 0x00FF) as u8;
                self.cpu.write_register(vx, byte);
            }
            0x8000..=0x8FFF => {
                let vx: u8 = ((opcode & 0x0F00) >> 8) as u8;
                let vy: u8 = ((opcode & 0x00F0) >> 4) as u8;
                let vx_val = self.cpu.read_register(vx);
                let vy_val = self.cpu.read_register(vy);
                println!("0x8 opcode {:#06X}", opcode);
                match opcode & 0x000F {
                    0x0 => self.cpu.write_register(vx, vy_val),
                    0x1 => {
                        self.cpu.write_register(vx, vx_val | vy_val);
                    }
                    0x2 => {
                        self.cpu.write_register(vx, vx_val & vy_val);
                    }
                    0x3 => {
                        self.cpu.write_register(vx, vx_val ^ vy_val);
                    }
                    0x4 => {
                        let sum = vx_val + vy_val;
                        if sum > 255 {
                            self.cpu.write_register(0xF, 1);
                        } else {
                            self.cpu.write_register(0xF, 0);
                        }
                        self.cpu.write_register(vx, sum & 0xFF);
                    }
                    0x5 => {
                        if vx_val > vy_val {
                            self.cpu.write_register(0xF, 1);
                        } else {
                            self.cpu.write_register(0xF, 0);
                        }
                        self.cpu.write_register(vx, vx_val.wrapping_sub(vy_val));
                    }
                    0x6 => {
                        self.cpu.write_register(0xF, vx_val & 0x1);
                        self.cpu.write_register(vx, vx_val >> 1);
                    }
                    0x7 => {
                        if vy_val > vx_val {
                            self.cpu.write_register(0xF, 1);
                        } else {
                            self.cpu.write_register(0xF, 0);
                        }
                        self.cpu.write_register(vx, vy_val.wrapping_sub(vx_val));
                    }
                    0xE => {
                        self.cpu.write_register(0xF, (vx_val & 0x80) >> 7);
                        self.cpu.write_register(vx, vx_val << 1);
                    }
                    _ => panic!("Unmatched 0x8 instruction {}", self.cpu.instruction_pointer),
                }
            }
            0x9000..=0x9FFF => {
                let vx = (opcode & 0x0F00) >> 8;
                let vy = (opcode & 0x00F0) >> 4;
                if self.cpu.read_register(vx as u8) != self.cpu.read_register(vy as u8) {
                    self.cpu.instruction_pointer += 2;
                }
            }
            0xA000..=0xAFFF => {
                println!("opcode {:#06X}", opcode);
                let address = opcode & 0x0FFF;
                self.cpu.write_address_register(address);
            }
            0xB000..=0xBFFF => {
                let address = opcode & 0x0FFF;
                let v0 = self.cpu.read_register(0) as u16;
                self.cpu.instruction_pointer = v0 + address;
            }
            0xC000..=0xCFFF => {
                println!("opcode {:#06X}", opcode);
                let vx = ((opcode & 0x0F00) >> 8) as u8;
                let byte = (opcode & 0x00FF) as u8;
                let random: u8 = rand::random();
                self.cpu.write_register(vx, byte & random);
            }
            0xD000..=0xDFFF => {
                println!("SKIPPING DISPLAY OPCODE");
            }
            0xE000..=0xEFFF => {
                println!("opcode {:#06X}", opcode);
            }
            0xF000..=0xFFFF => {
                println!("opcode {:#06X}", opcode);
                match (opcode & 0x00FF) as u8 {
                    0x07 => {}
                    0x0A => {}
                    0x15 => {}
                    0x18 => {}
                    0x1E => {}
                    0x29 => {}
                    0x33 => {}
                    0x55 => {}
                    0x65 => {}
                    _ => {}
                }
            }

            _ => {
                println!("instruction: {}, testar", opcode)
            }
        }
    }
}

impl Default for Chip8 {
    fn default() -> Self {
        Chip8 {
            cpu: Cpu::new(),
            memory: Ram::new(),
        }
    }
}
