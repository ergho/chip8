use std::thread;
mod cpu;
mod emu;
mod mem;
const START_ADDR: u16 = 0x200;
const FONT_ADDR: usize = 0x50;
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut emulator = emu::Chip8::default();
    emulator.load_file("roms/15PUZZLE")?;

    loop {
        emulator.read_thingymajigigigigigigigies();
        //thread::sleep_ms(200);
    }
}
