use crate::START_ADDR;

pub struct Cpu {
    ///V0-VF
    registers: [u8; 16],
    address_register: u16,
    pub instruction_pointer: u16,
    // maybe use a Vector instead cause easier?
    pub stack: [u16; 16],
    pub sp: u8,
    dt: u8,
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            registers: [0; 16],
            address_register: 0,
            instruction_pointer: START_ADDR,
            stack: [0; 16],
            sp: 0,
            dt: 0,
        }
    }

    pub fn read_register(&self, register: u8) -> u8 {
        self.registers[register as usize]
    }

    pub fn write_register(&mut self, register: u8, value: u8) {
        self.registers[register as usize] = value;
    }

    pub fn read_address_register(&self) -> u16 {
        self.address_register
    }

    pub fn write_address_register(&mut self, address: u16) {
        self.address_register = address;
    }
}

//#[derive(Clone, Copy, Debug)]
//#[repr(usize)]
//pub enum Register {
//    V0,
//    V1,
//    V2,
//    V3,
//    V4,
//    V5,
//    V6,
//    V7,
//    V8,
//    V9,
//    VA,
//    VB,
//    VC,
//    VD,
//    VE,
//    VF,
//}
//
//impl From<u8> for Register {
//    fn from(val: u8) -> Self {
//        assert!(val < 0x10);
//        val.into()
//    }
//}
